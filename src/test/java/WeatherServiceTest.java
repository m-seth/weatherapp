import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.pactera.weatherapp.WeatherData;
import com.pactera.weatherapp.service.WeatherService;

import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceTest extends TestCase {
	
	@InjectMocks
	WeatherService service;
	
	@Test
	public void shouldGetWeatherData() {
		WeatherData data = service.getWeatherData("Melbourne");
		assertNotNull(data);
	}

}
