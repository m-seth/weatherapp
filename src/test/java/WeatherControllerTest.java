import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.pactera.weatherapp.WeatherData;
import com.pactera.weatherapp.config.WeatherAppConfig;
import com.pactera.weatherapp.controller.WeatherController;
import com.pactera.weatherapp.service.WeatherService;

@RunWith(MockitoJUnitRunner.class)
public class WeatherControllerTest {

	@Mock
	private WeatherService weatherService;

	@Mock
	private WeatherAppConfig config;

	@Mock
	private WeatherData weatherDataMock;

	@InjectMocks
	private WeatherController weatherController;

	@Test
	public void shouldDisplayWeather() {
		// given
		Mockito.when(weatherService.getWeatherData(Mockito.anyString())).thenReturn(weatherDataMock);

		// when
		ModelAndView modelView = weatherController.displayWeather("Melbourne");

		// then
		assertNotNull(modelView.getModel().get("weatherData"));
		assertNotNull(modelView.getModel().get("message"));
		assertNotNull(modelView.getModel().get("city"));

		assertEquals("Melbourne", modelView.getModel().get("city"));

	}

}
