package com.pactera.weatherapp.controller;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pactera.weatherapp.WeatherData;
import com.pactera.weatherapp.config.WeatherAppConfig;
import com.pactera.weatherapp.service.WeatherService;

/**
 * Controller class.
 * 
 * @author mseth
 * 
 *
 */
@Controller
public class WeatherController {

	private static final Logger logger = Logger.getLogger(WeatherController.class);

	private final String message = "Weather Forecast for : ";

	private WeatherService weatherService;
	private WeatherAppConfig config;
	
	@Autowired
	private Environment env;

	@Autowired
	public WeatherController(WeatherService weatherService, WeatherAppConfig config) {
		this.weatherService = weatherService;
		this.config = config;
	}

	@RequestMapping("/welcome")
	public ModelAndView welcome() {

		ModelAndView mv = new ModelAndView("welcome");
		mv.addObject("cityList", getCityList());
		return mv;
	}

	@RequestMapping("/displayWeather")
	public ModelAndView displayWeather(@RequestParam(value = "selectedCity", required = true) String city) {

		WeatherData weatherData = weatherService.getWeatherData(city);
		weatherData.setUpdatedTime(getCurrentDate());

		logger.info("weatherData : " + weatherData.toString());
		ModelAndView mv = new ModelAndView("weatherdisplay");
		mv.addObject("message", message);
		mv.addObject("city", city);
		mv.addObject("weatherData", weatherData);

		return mv;
	}

	/**
	 * Reads the cities from configuration property
	 * 
	 * @return List of city names supported by the application
	 */
	private List<String> getCityList() {
		return Arrays.asList(env.getProperty("weatherApp.cities").split(","));

	}

	/**
	 * Get the current date
	 * 
	 * @return current date in String format
	 */
	private String getCurrentDate() {
		Date now = new Date();
		return DateFormat.getInstance().format(now);

	}

}
