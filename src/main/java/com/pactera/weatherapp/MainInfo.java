package com.pactera.weatherapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO for Weather Main Data.
 * 
 * @author mseth
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MainInfo {

	@JsonProperty("temp")
	private String temp;

	@JsonProperty("pressure")
	private String pressure;

	@JsonProperty("humidity")
	private String humidity;

	@JsonProperty("temp_min")
	private String tempMin;

	@JsonProperty("emp_max")
	private String tempMax;

	public String getTemp() {
		return temp;
	}

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public String getPressure() {
		return pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getTempMin() {
		return tempMin;
	}

	public void setTempMin(String tempMin) {
		this.tempMin = tempMin;
	}

	public String getTempMax() {
		return tempMax;
	}

	public void setTempMax(String tempMax) {
		this.tempMax = tempMax;
	}

	@Override
	public String toString() {
		return "MainInfo [temp=" + temp + ", pressure=" + pressure + ", humidity=" + humidity + ", tempMin=" + tempMin
				+ ", tempMax=" + tempMax + "]";
	}

}
