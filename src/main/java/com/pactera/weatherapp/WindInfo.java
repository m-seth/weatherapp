package com.pactera.weatherapp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO for WIND Data
 * 
 * @author mseth
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WindInfo {

	@JsonProperty("speed")
	private String speed;

	@JsonProperty("deg")
	private String deg;

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public String getDeg() {
		return deg;
	}

	public void setDeg(String deg) {
		this.deg = deg;
	}

	@Override
	public String toString() {
		return "WindInfo [speed=" + speed + ", deg=" + deg + "]";
	}

}
