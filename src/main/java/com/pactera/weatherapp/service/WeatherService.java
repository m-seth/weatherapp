package com.pactera.weatherapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.pactera.weatherapp.WeatherData;

/**
 * 
 * @author mseth Service class to fetch Dynamic Weather Data.
 */
@Service
public class WeatherService {

	private static final Logger logger = Logger.getLogger(WeatherService.class);

	public WeatherData getWeatherData(String city) {
		logger.info("getWeatherData");

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl("http://api.openweathermap.org/data/2.5/weather").queryParam("q", city + ",au")
				.queryParam("appid", "4cf6d685ba10bf67d3854778a4b9ba77");

		WeatherData response = getRestTemplate().getForObject(builder.build().encode().toUri(), WeatherData.class);

		return response;

	}

	private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();

		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
		supportedMediaTypes.add(MediaType.ALL);

		messageConverters.add(new MappingJackson2HttpMessageConverter());
		messageConverters.add(new FormHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		return restTemplate;
	}

}
