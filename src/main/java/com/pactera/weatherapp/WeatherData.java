package com.pactera.weatherapp;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The POJO for Weather Data
 * 
 * @author mseth
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherData {

	@JsonProperty("name")
	private String name;

	@JsonIgnore
	private String updatedTime;

	@JsonProperty("weather")
	private WeatherInfo[] weatherInfo;

	@JsonProperty("main")
	private MainInfo mainInfo;

	@JsonProperty("wind")
	private WindInfo windInfo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public WeatherInfo[] getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(WeatherInfo[] weatherInfo) {
		this.weatherInfo = weatherInfo;
	}

	public MainInfo getMainInfo() {
		return mainInfo;
	}

	public void setMainInfo(MainInfo mainInfo) {
		this.mainInfo = mainInfo;
	}

	public WindInfo getWindInfo() {
		return windInfo;
	}

	public void setWindInfo(WindInfo windInfo) {
		this.windInfo = windInfo;
	}

	@Override
	public String toString() {
		return "WeatherData [name=" + name + ", updatedTime=" + updatedTime + ", weatherInfo="
				+ Arrays.toString(weatherInfo) + ", mainInfo=" + mainInfo + ", windInfo=" + windInfo + "]";
	}

}
