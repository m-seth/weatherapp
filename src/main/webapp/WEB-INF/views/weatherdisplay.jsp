<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Weather Display</title>
</head>
<body>
	<center>
		<h2>Current Weather Conditions</h2>
		<h2>${message} ${city}</h2>

		<c:if test="${not empty weatherData}">
			<table>
				<tr>
					<td>City</td>
					<td>${weatherData.name}</td>
				</tr>
				<tr>
					<td>UpdatedTime</td>
					<td>${weatherData.updatedTime}</td>
				</tr>
				<tr>
					<td>Weather</td>
					<td>${weatherData.weatherInfo[0].description}</td>
				</tr>
				<tr>
					<td>Temperature</td>
					<td>${weatherData.mainInfo.temp}</td>
				</tr>
				<tr>
					<td>Wind</td>
					<td>${weatherData.windInfo.speed} Km/h</td>
				</tr>
			</table>
		</c:if>

	</center>
</body>
</html>