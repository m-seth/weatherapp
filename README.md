# README #

### Weather App ###

This is a web application to display current weather for 3 Australian cities: Sydney,
Melbourne and Wollongong. The user selects the city from the dropdown list on web page for city selection,
when city is changed corresponding real-time weather information is displayed.

### How do I get set up? ###

This is a maven project using Spring MVC with all the dependecies defined in pom.xml.
The list of cities are configurable and is defined in the property file.

To build the project (using default profile):
mvn clean install : A new WAR file will be generated at project/target/WeatherApp.war, just copy and deploy to your Tomcat.

After war is deployed hit the URL:
http://localhost:8080/WeatherApp/

### Who do the App talk to? ###

The application is using online weather API available through : api.openweathermap.org